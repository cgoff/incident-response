@echo off
REM Copyright (c) 2018 Chris Goff <cgoff@fastmail.fm>
REM
REM Permission to use, copy, modify, and distribute this software for any
REM purpose with or without fee is hereby granted, provided that the above
REM copyright notice and this permission notice appear in all copies.
REM
REM THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
REM WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
REM MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
REM ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
REM WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
REM ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
REM OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

REM Version 0.3 (beta)

setlocal EnableDelayedExpansion
reg Query "HKLM\Hardware\Description\System\CentralProcessor\0" | find /i "x86" >> NUL && set OS=32BIT || set OS=64BIT

:mainmenu
echo #############################################
echo # The Poor Man's Incident Response Tool Kit #
echo #############################################
echo.
echo 1. Set path for data export
echo 2. Start Packet Capture
echo 3. Stop Packet Capture
echo 4. Gather Data Lite (low resource impact)
echo 5. Gather Data Full
echo 6. Image Memory
echo 7. Image Local Disk (C:\)
echo 8. Exit
echo.
echo NOTE: Set path first!
echo.
choice /C:12345678 /N /M "Choose a function: " 
if errorlevel 8 GOTO cleanup
if errorlevel 7 GOTO diskimage
if errorlevel 6 GOTO memoryimage
if errorlevel 5 GOTO gatherdata_full
if errorlevel 4 GOTO gatherdata_lite
if errorlevel 3 GOTO stoppacketcapture
if errorlevel 2 GOTO packetcapture
if errorlevel 1 GOTO set_export_path

:set_export_path
mkdir C:\Windows\Temp\export-data
set "host_path=C:\Windows\Temp\export-data"

REM Set the path to where you want the data stored (local or mapped network drive)
set /p "export_path=Enter path to export data to and press ENTER: "
REM net use %export_path% /p:no
GOTO mainmenu

:packetcapture
netsh trace start capture=yes report=yes maxsize=2048 tracefile=%export_path%\packet-capture.etl
GOTO mainmenu

:stoppacketcapture
netsh trace stop
GOTO mainmenu

:gatherdata_lite
REM Reference gatherdata_full remarks for information
mkdir %export_path%\gatherdata_lite
set "lite_export_path=%export_path%\gatherdata_lite"
echo %lite_export_path%
echo Generated on: %DATE% %TIME% by >> %lite_export_path%\host.information.txt
whoami >> %lite_export_path%\host.information.txt
systeminfo >> %lite_export_path%\host.information.txt

ipconfig /allcompartments /all >> %lite_export_path%/host.network-config.txt
netstat -nr >> %lite_export_path%\host.network-connections.txt
netstat -vb >> %lite_export_path%\host.network-connections.txt
net use >> %lite_export_path%\host.network-share-drives.txt
net session >> %lite_export_path%\host.network-connections.txt
net view \\127.0.0.1 >> %lite_export_path%\host.network-shared-resources.txt
nbtstat -S >> %lite_export_path%\host.network-netbios.txt
route print >> %lite_export_path%\host.network-routes.txt
arp -a >> %lite_export_path%\host.network-arp.txt
netsh wlan show interfaces >> %lite_export_path%\host.network-config.txt
netsh wlan show all >> %lite_export_path%\host.network-config.txt

ipconfig /displaydns >> %lite_export_path%\host.network-dns.txt
more %SystemRoot%\System32\Drivers\etc\hosts >> %lite_export_path%\host.network-hosts-file.txt

net users >> %lite_export_path%\host.users-local.txt
net localgroup administrators >> %lite_export_path%\host.groups-local.txt

GOTO mainmenu

:gatherdata_full
REM ***********************************
REM Gather Host Identifying Information
echo Gathering Host Information...
echo %DATE% %TIME% >> %export_path%\host.information.txt
systeminfo >> %export_path%\host.information.txt
whoami >> %export_path%\host.information.txt
tree /F /A C:\ >> %host_path%\host.drive-tree.txt
copy %host_path%\host.drive-tree.txt %export_path%
REM ***********************************

REM **************************
REM Export service permissions
for /f "tokens=2 delims='='" %%a in ('wmic service list full^|find /i "pathname"^|find /i /v "system32"') do @echo %%a >> %export_path%\service-permissions.tmp
for /f eol^=^"^ delims^=^" %%a in (%export_path%\service-permissions.tmp) do cmd.exe /c icacls "%%a" >> %export_path%\host.service-permissions.txt
REM **************************

REM *****************************
REM Get list of installed patches
echo Gathering installed patches...
wmic qfe list >> %export_path%\host.installed-patches.txt
REM *****************************

REM ******************************
REM Get list of installed software
echo Gathering installed sofware...
wmic product list >> %export_path%\host.installed-software.txt
REM ******************************

REM *******************
REM Get list of drivers
echo Gathering list of installed drivers...
driverquery >> %export_path%\host.installed-drivers.txt
REM *******************

REM ****************
REM Extract Registry
echo Gathering all registry keys...
reg export HKLM %export_path%\hklm.reg
reg export HKCU %export_path%\hkcu.reg
reg export HKCR %export_path%\hkcr.reg
reg export HKU %export_path%\hku.reg
reg export HKCC %export_path%\hkcc.reg
REM ****************

REM **************************
REM Gather Network Information
ipconfig /allcompartments /all >> %export_path%/host.network-config.txt
netstat -naob >> %export_path%\host.network-connections.txt
netstat -nr >> %export_path%\host.network-connections.txt
netstat -vb >> %export_path%\host.network-connections.txt
net use >> %export_path%\host.network-share-drives.txt
net session >> %export_path%\host.network-connections.txt
net view \\127.0.0.1 >> %export_path%\host.network-shared-resources.txt
nbtstat -S >> %export_path%\host.network-netbios.txt
route print >> %export_path%\host.network-routes.txt
arp -a >> %export_path%\host.network-arp.txt
netsh wlan show interfaces >> %export_path%\host.network-config.txt
netsh wlan show all >> %export_path%\host.network-config.txt
wmic nicconfig get description,IPAddress,MACaddress >> %export_path%\host.network-config.txt
wmic ntdomain list brief >> %export_path%\host.network-connections.txt
REM **************************

REM **********************
REM Gather DNS information
ipconfig /displaydns >> %export_path%\host.network-dns.txt
more %SystemRoot%\System32\Drivers\etc\hosts >> %export_path%\host.network-hosts-file.txt
REM **********************

REM ***********************************************
REM Gather Services, Task and Processes information
net start >> %export_path%\host.services-started.txt
tasklist >> %export_path%\host.tasks-running.txt
tasklist /svc >> %export_path%\host.tasks-running.txt

sc query >> %export_path%\host.services.txt
wmic service list config >> %export_path%\host.services.txt

wmic process list >> %export_path%\host.processes.txt
wmic process list status >> %export_path%\host.processes.txt
wmic process list memory >> %export_path%\host.processes.txt
wmic job list brief >> %export_path%\host.processes.txt

tasklist >> %export_path%\host.tasklist.txt
tasklist /svc >> %export_path%\host.tasklist.txt
schtasks >> %export_path%\host.scheduled-tasks.txt
REM ***********************************************

REM *******************************
REM Gather Host Startup information
wmic startup list brief >> %export_path%\host.startup-list.txt
REM *******************************

REM *****************
REM Gather Event Logs
wevtutil qe security /f:text >> %host_path%\host.eventlog-security.txt
wevtutil qe system /f:text >> %host_path%\host.eventlog-system.txt
copy %host_path%\host.eventlog-security.txt %export_path%
copy %host_path%\host.eventlog-system.txt %export_path%
REM *****************

REM *********************************************
REM Gather Local User and Local Group Information
net users >> %export_path%\host.users-local.txt
net localgroup administrators >> %export_path%\host.groups-local.txt
REM *********************************************

REM *********************
REM Run SysInternals Tools
sysinternals\tcpvcon.exe -a /accepteula >> %export_path%\host.network-tcp-connections.txt
sysinternals\loggedon.exe /accepteula >> %export_path%\host.users-loggedon.txt
sysinternals\logonsessions.exe /accepteula >> %export_path%\host.users-loggon-sessions.txt
sysinternals\pslist.exe /accepteula >> %export_path%\host.processes.txt
sysinternals\handle.exe /accepteula >> %export_path%\host.processes-handles.txt
sysinternals\listdlls.exe /accepteula >> %export_path%\host.listdlls.txt
sysinternals\autorunsc.exe /accepteula \* >> %export_path%\host.autoruns.txt
REM *********************
GOTO mainmenu

REM ************
REM Memory Image
:memoryimage
echo ################
echo # Memory Image #
echo ################
echo.
echo 1. Image Memory
echo 2. Image Memory w/PageFile
echo 3. Create ELF core dump
echo 4. Exit to Main Menu
echo.
choice /C:1234 /N /M "Choose a function: " 
if errorlevel 4 GOTO mainmenu
if errorlevel 3 GOTO memoryimage_elf
if errorlevel 2 GOTO memoryimage_pagefile
if errorlevel 1 GOTO memoryimage_standard

:memoryimage_standard
winpmem\winpmem.exe %export_path%\mem-image.raw
GOTO memoryimage

:memoryimage_pagefile
winpmem\winpmem.exe -p %export_path%\mem-image-with-pagefile.raw
GOTO memoryimage

:memoryimage_elf
winpmem\winpmem.exe -e %export_path%\mem-coredump.raw
GOTO memoryimage
REM ************

:diskimage
echo Warning: Disk images can be several hundred GBs in size.
choice /N /T 30 /D N /M "Are you sure you want to gather a disk image (Y/N)?"
IF errorlevel 2 goto mainmenu
IF errorlevel 1 goto start_diskimage
:start_diskimage
ftkimager\ftkimager C: %export_path%\hdd-image)
GOTO mainmenu

:cleanup
REM ********
REM Clean up
REM net use %export_path% /delete
REM del %export_path%\service-permissions.tmp
rmdir /Q /S %host_path%
REM ********
echo.
exit /b